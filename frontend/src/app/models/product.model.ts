import { Product } from './Product';

export interface ProductModel {
  [key: string]: Product[];
}
