import { Component, Input, OnInit } from '@angular/core';
import { Product } from '../../../models/Product';

@Component({
  selector: 'app-product-list-table',
  templateUrl: './product-list-table.component.html',
  styleUrls: ['./product-list-table.component.scss'],
})
export class ProductListTableComponent implements OnInit {
  @Input()
  public products: Product[] | undefined;
  @Input()
  public totalPrice: any;

  constructor() {}

  ngOnInit(): void {}
}
