import { Ingredient } from 'src/app/models/Ingredient';
import { Product } from 'src/app/models/Product';
import { User } from 'src/app/models/User';

export interface IProductsState {
  products: Product[];
  user: User | null | string;
  productHash: Map<string, Product[]> | null;
  selectedIngredients: Ingredient[];
}

export const initialProductsState: IProductsState = {
  products: [],
  user: localStorage.getItem('userData'),
  productHash: null,
  selectedIngredients: [],
};
