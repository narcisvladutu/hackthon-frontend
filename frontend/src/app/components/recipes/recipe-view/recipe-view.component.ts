import { Component, Input, OnInit } from '@angular/core';
import { Recipe } from 'src/app/models/Recipe';

@Component({
  selector: 'app-recipe-view',
  templateUrl: './recipe-view.component.html',
  styleUrls: ['./recipe-view.component.scss'],
})
export class RecipeViewComponent implements OnInit {
  @Input()
  recipe: Recipe | undefined;

  image: string | null = localStorage.getItem('recipeImage');
  description: string | null = localStorage.getItem('recipeDescription');
  name: string | null = localStorage.getItem('recipeName');

  constructor() {}

  ngOnInit(): void {}
}
