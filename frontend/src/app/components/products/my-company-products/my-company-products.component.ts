import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/Product';
import { ProductService } from 'src/app/services/product.service';

@Component({
  selector: 'app-my-company-products',
  templateUrl: './my-company-products.component.html',
  styleUrls: ['./my-company-products.component.scss'],
})
export class MyCompanyProductsComponent implements OnInit {
  products: Product[] | undefined;

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    let username = localStorage.getItem('userName');
    if (username !== null)
      this.http
        .get<Product[]>(
          'http://localhost:8081/products/company=' +
            encodeURIComponent(username)
        )
        .subscribe((products) => {
          this.products = products;
        });
  }
}
