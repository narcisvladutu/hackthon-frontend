import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Ingredient } from 'src/app/models/Ingredient';
import { Recipe } from 'src/app/models/Recipe';
import { selectSelectedIngredients } from 'src/app/store/products/products-selectors';
import { AppState } from 'src/app/store/state/app.state';

@Component({
  selector: 'app-recipes-list',
  templateUrl: './recipes-list.component.html',
  styleUrls: ['./recipes-list.component.scss'],
})
export class RecipesListComponent implements OnInit {
  ingredients: Ingredient[] = [];
  recipes: Recipe[] | undefined;

  constructor(private store: Store<AppState>, private http: HttpClient) {}

  ngOnInit(): void {
    this.store.select(selectSelectedIngredients).subscribe((data) => {
      this.ingredients = data;
      this.http
        .post<Recipe[]>(
          'http://localhost:8081/recipes/recipes-by-ingredients',
          data
        )
        .subscribe((d) => {
          this.recipes = d;
        });
    });
  }
}
