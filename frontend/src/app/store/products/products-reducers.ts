import { initialProductsState } from './products-state';
import { createReducer, on } from '@ngrx/store';
import {
  addProductHash,
  addProductToShoppingList,
  addSelectedIngredients,
  addUser,
  clearShoppingList,
  deleteUser,
} from './products-actions';

export const productsReducer = createReducer(
  initialProductsState,
  // Handle successfully loaded todos
  on(addProductToShoppingList, (state, { product }) => ({
    ...state,
    products: [...state.products, product],
    error: null,
    status: 'success',
  })),
  on(addUser, (state, { user }) => ({
    ...state,
    user: user,
    error: null,
    status: 'success',
  })),
  on(addProductHash, (state, { productHash }) => ({
    ...state,
    productHash: productHash,
    error: null,
    status: 'success',
  })),
  on(addSelectedIngredients, (state, { selectedIngredients }) => ({
    ...state,
    selectedIngredients: selectedIngredients,
    error: null,
    status: 'success',
  })),
  on(deleteUser, (state, {}) => ({
    ...state,
    user: null,
    error: null,
    status: 'success',
  })),
  on(clearShoppingList, (state, any) => ({
    ...state,
    products: [],
  }))
);
