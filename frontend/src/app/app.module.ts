import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductListComponent } from './components/products/product-list/product-list.component';
import { ProductComponent } from './components/products/product/product.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatExpansionModule } from '@angular/material/expansion';

import { FlexLayoutModule } from '@angular/flex-layout';

import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { MatButtonModule } from '@angular/material/button';
import { RecipesComponent } from './components/recipes/recipes/recipes.component';
import { MyShoppingListComponent } from './components/orders/my-shopping-list/my-shopping-list.component';
import { HeaderComponent } from './components/header/header.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AddProductToShoppingListComponent } from './components/products/add-product-to-shopping-list/add-product-to-shopping-list.component';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import { ProductsEffects } from './store/products/products-effects';
import { productsReducer } from './store/products/products-reducers';
import { RecipesListComponent } from './components/recipes/recipes-list/recipes-list.component';
import { RecipeCardComponent } from './components/recipes/recipe-card/recipe-card.component';
import { RecipeViewComponent } from './components/recipes/recipe-view/recipe-view.component';
import { ProductEditComponent } from './components/products/product-edit/product-edit.component';
import { OrdersComponent } from './components/orders/orders/orders.component';
import { ProductListTableComponent } from './components/orders/product-list-table/product-list-table.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { MyCompanyProductsComponent } from './components/products/my-company-products/my-company-products.component';
import { AddProductComponent } from './components/products/add-product/add-product.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { DatePickerComponent } from './components/utils/date-picker/date-picker.component';
import { MatNativeDateModule } from '@angular/material/core';
import { MatIconModule } from '@angular/material/icon';
import { MyProductComponent } from './components/products/my-product/my-product.component';
import { MyOrdersComponent } from './components/orders/my-orders/my-orders.component';
import { PlaceOrderDialogComponent } from './components/orders/place-order-dialog/place-order-dialog.component';
import { ConfirmDialogComponent } from './components/orders/confirm-dialog/confirm-dialog.component';
import { DeleteProductDialogComponent } from './components/products/delete-product-dialog/delete-product-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductListComponent,
    ProductComponent,
    RecipesComponent,
    MyShoppingListComponent,
    HeaderComponent,
    AddProductToShoppingListComponent,
    RecipesListComponent,
    RecipeCardComponent,
    RecipeViewComponent,
    ProductEditComponent,
    OrdersComponent,
    ProductListTableComponent,
    LogInComponent,
    UserProfileComponent,
    MyCompanyProductsComponent,
    AddProductComponent,
    MyOrdersComponent,
    MyProductComponent,
    DatePickerComponent,
    PlaceOrderDialogComponent,
    ConfirmDialogComponent,
    DeleteProductDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FlexLayoutModule,
    MatCardModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    MatIconModule,
    MatFormFieldModule,
    MatButtonModule,
    MatExpansionModule,
    MatCheckboxModule,
    BrowserAnimationsModule,
    MatDialogModule,
    StoreModule,
    MatInputModule,
    MatNativeDateModule,
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    StoreRouterConnectingModule.forRoot(),
    StoreModule.forRoot(
      {
        products: productsReducer,
      },
      {}
    ),
    EffectsModule.forRoot([ProductsEffects]),
    FormsModule,
  ],

  providers: [],
  bootstrap: [AppComponent],
  exports: [HeaderComponent, HeaderComponent],
})
export class AppModule {}
