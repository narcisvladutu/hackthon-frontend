import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { OrderModelUpdate } from '../models/order.model';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  constructor(private http: HttpClient) {}

  getOrdersByCompany(company: string): Observable<OrderModelUpdate[]> {
    return this.http.get<OrderModelUpdate[]>(
      'http://localhost:8081/order/company-name/name=' +
        encodeURIComponent(company)
    );
  }
}
