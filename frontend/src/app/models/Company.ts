import { Product } from './Product';
import { UserRole } from './UserRole.enum';

export interface Company {
  id: number;
  email: string;
  username: string;
  password: string;
  image: string;
  role: UserRole;
  products: Product[];
}
