export interface ProductSave {
  name: string;
  price: number;
  image: string;
  expire_date: string;
  stock: number;
  companyName: string;
}
