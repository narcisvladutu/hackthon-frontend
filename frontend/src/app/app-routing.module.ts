import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyShoppingListComponent } from './components/orders/my-shopping-list/my-shopping-list.component';
import { ProductListComponent } from './components/products/product-list/product-list.component';
import { RecipesComponent } from './components/recipes/recipes/recipes.component';
import { OrdersComponent } from './components/orders/orders/orders.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { AuthService } from './auth/auth.service';
import { MyCompanyProductsComponent } from './components/products/my-company-products/my-company-products.component';
import { AddProductComponent } from './components/products/add-product/add-product.component';
import { MyOrdersComponent } from './components/orders/my-orders/my-orders.component';
import { RecipesListComponent } from './components/recipes/recipes-list/recipes-list.component';
import { RecipeViewComponent } from './components/recipes/recipe-view/recipe-view.component';

const routes: Routes = [
  {
    path: 'products',
    component: ProductListComponent,
    canActivate: [AuthService],
  },
  {
    path: 'recipes',
    component: RecipesComponent,
    canActivate: [AuthService],
  },
  {
    path: 'my-shopping-list',
    component: MyShoppingListComponent,
    canActivate: [AuthService],
  },
  {
    path: 'log-in',
    component: LogInComponent,
  },
  {
    path: 'my-products',
    component: MyCompanyProductsComponent,
    canActivate: [AuthService],
  },
  {
    path: 'add-product',
    component: AddProductComponent,
    canActivate: [AuthService],
  },
  {
    path: 'my-orders',
    component: MyOrdersComponent,
    canActivate: [AuthService],
  },
  {
    path: '',
    redirectTo: '/log-in',
    pathMatch: 'full',
  },
  {
    path: 'place-order',
    component: OrdersComponent,
    canActivate: [AuthService],
  },
  {
    path: 'recipes-filtered',
    component: RecipesListComponent,
    canActivate: [AuthService],
  },
  {
    path: 'recipe',
    component: RecipeViewComponent,
    canActivate: [AuthService],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
