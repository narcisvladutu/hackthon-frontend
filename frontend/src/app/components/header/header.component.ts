import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Path } from 'src/app/models/Path';
import { UserRole } from 'src/app/models/UserRole.enum';
import { AppState } from 'src/app/store/state/app.state';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  path: typeof Path = Path;
  image: string | null = null;
  username: string | null = null;
  userrole: string | null = null;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {
    if (localStorage.getItem('userImage') !== null) {
      this.image = localStorage.getItem('userImage');
      this.username = localStorage.getItem('userName');
      this.userrole = localStorage.getItem('userRole');
    }
  }
}
