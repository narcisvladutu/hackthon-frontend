import { Component, OnInit } from '@angular/core';
import { AppState } from 'src/app/store/state/app.state';
import { Store } from '@ngrx/store';
import { Product } from 'src/app/models/Product';
import { selectAllProducts } from 'src/app/store/products/products-selectors';
import { Router } from '@angular/router';
import { addProductHash } from '../../../store/products/products-actions';
import { MatDialog } from '@angular/material/dialog';
import { PlaceOrderDialogComponent } from '../place-order-dialog/place-order-dialog.component';

@Component({
  selector: 'app-my-shopping-list',
  templateUrl: './my-shopping-list.component.html',
  styleUrls: ['./my-shopping-list.component.scss'],
})
export class MyShoppingListComponent implements OnInit {
  myProducts: Product[] = [];
  asd?: Product[] = [];
  totalPrice: number = 0;
  public productsHash: Map<string, Product[]> = new Map<string, Product[]>();
  partialPrices?: number;

  constructor(
    private dialog: MatDialog,
    private store: Store<AppState>,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.store.select(selectAllProducts).subscribe((products) => {
      this.myProducts = products;
      products.forEach((product) => (this.totalPrice += product.price));
    });
    this.myProducts.forEach((product) => {
      this.asd = [];
      if (this.productsHash.has(product.companyName)) {
        this.asd = this.productsHash.get(product.companyName);
        this.asd?.push(product);
        if (this.asd != undefined) {
          this.productsHash.set(product.companyName, this.asd);
        }
      } else {
        this.asd?.push(product);
        if (this.asd != undefined) {
          this.productsHash.set(product.companyName, this.asd);
        }
      }
    });
  }

  placeOrder() {
    let dialogRef = this.dialog.open(PlaceOrderDialogComponent, {
      height: '200px',
      width: '400px',
      enterAnimationDuration: '400ms',
      exitAnimationDuration: '300ms',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'DA') {
        this.store.dispatch(addProductHash({ productHash: this.productsHash }));
        this.router.navigateByUrl('/place-order');
      }
    });
  }

  getKeys(productsHash: Map<string, Product[]>) {
    return Array.from(productsHash.keys());
  }

  getPrice(products: Product[]): number {
    let price: number = 0;
    products.forEach((product) => {
      price += product.price;
    });
    this.partialPrices = price;
    return price;
  }
}
