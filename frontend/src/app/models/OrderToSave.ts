import { Product } from './Product';
import { StatusEnum } from './StatusEnum';

export interface OrderToSave {
  name: String;
  description: String;
  status: StatusEnum;
  createdDate: Date;
  price: number;
  products: Map<string, Product[]>;
  idUser: number;
  username: String;
}
