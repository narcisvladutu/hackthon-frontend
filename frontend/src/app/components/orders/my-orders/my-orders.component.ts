import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../../services/order.service';
import { OrderModelUpdate } from '../../../models/order.model';

@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.scss'],
})
export class MyOrdersComponent implements OnInit {
  orders: OrderModelUpdate[] | undefined;

  constructor(private orderService: OrderService) {}

  ngOnInit(): void {
    let username = localStorage.getItem('userName');
    if (username) {
      this.orderService.getOrdersByCompany(username).subscribe((orders) => {
        this.orders = orders;
      });
    }
  }
}
