import { UserRole } from './UserRole.enum';

export interface User {
  id: number;
  email: string;
  username: string;
  password: string;
  image: string;
  role: UserRole;
}
