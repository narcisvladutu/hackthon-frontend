export interface OrderModel {
  phone: string,
  firstName: string,
  lastName: string,
  country: string,
  town: string,
  state: string
}
export interface OrderModelUpdate{
  name:string,
  description:string,
  status:string,
  price:string,
}
