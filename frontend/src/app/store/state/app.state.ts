import { IProductsState } from '../products/products-state';

export interface AppState {
  products: IProductsState;
}
