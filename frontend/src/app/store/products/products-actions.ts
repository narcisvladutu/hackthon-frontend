import { createAction, props } from '@ngrx/store';
import { Ingredient } from 'src/app/models/Ingredient';
import { Product } from 'src/app/models/Product';
import { User } from 'src/app/models/User';

export const addProductToShoppingList = createAction(
  '[Products] Add Product To Shopping List',
  props<{ product: Product }>()
);

export const addProductToShoppingListSuccess = createAction(
  '[Products] Add Product To Shopping List Success',
  props<{ product: Product }>()
);

export const addUser = createAction(
  '[Products] Add User',
  props<{ user: User }>()
);

export const addProductHash = createAction(
  '[Products] Add Product Has',
  props<{ productHash: Map<string, Product[]> }>()
);

export const deleteUser = createAction('[Products] Delete User');

export const addSelectedIngredients = createAction(
  '[Products] Add Selected Ingredients',
  props<{ selectedIngredients: Ingredient[] }>()
);

export const clearShoppingList = createAction(
  '[Products] Clear Shopping List',
  props<any>()
);
