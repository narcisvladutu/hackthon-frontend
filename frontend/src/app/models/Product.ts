export interface Product {
  id: number;
  name: string;
  price: number;
  image: string;
  expire_date: string;
  stock: number;
  companyName: string;
}
