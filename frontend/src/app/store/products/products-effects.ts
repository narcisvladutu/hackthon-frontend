import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { mergeMap, tap } from 'rxjs';
import { AppState } from '../state/app.state';
import { addSelectedIngredients } from './products-actions';

@Injectable()
export class ProductsEffects {
  constructor(
    private actions$: Actions,
    private store: Store<AppState>,
    private router: Router
  ) {}

  addSelectedIngredients$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(addSelectedIngredients),
        tap(() => {
          this.router.navigateByUrl('/recipes-filtered');
        })
      ),
    { dispatch: false }
  );
}
