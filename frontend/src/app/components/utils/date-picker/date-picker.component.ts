import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Moment } from 'moment';
import * as moment from 'moment';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
})
export class DatePickerComponent implements OnInit {
  @Input() isRequired?: boolean = false;
  @Input() label? = 'Selecteaza data';
  @Input() minDate?: string;
  @Input() maxDate?: string;
  @Input() disablePastDates?: boolean;
  @Output() updateDayDateChange = new EventEmitter<Moment>();

  minDateMoment: Moment | null = null;
  maxDateMoment: Moment | null = null;
  validatorRemoved = false;
  dayDateField = new FormControl();

  @Input()
  set dayDate(value: string) {
    this.dayDateField.setValue(value ? moment(value, 'DD/MM/YYYY') : null);
  }

  ngOnInit(): void {
    this.minDateMoment = this.minDate
      ? moment(this.minDate, 'DD/MM/YYYY')
      : null;
    this.maxDateMoment = this.maxDate
      ? moment(this.maxDate, 'DD/MM/YYYY')
      : null;

    if (
      this.disablePastDates &&
      (!this.minDateMoment ||
        moment().startOf('day').isAfter(this.minDateMoment))
    ) {
      this.minDateMoment = moment().startOf('day');
      this.minDate = this.minDateMoment.format('DD/MM/YYYY');
    }
    if (this.isRequired) this.dayDateField.setValidators([Validators.required]);
  }

  onValueChanged(): void {
    if (this.dayDateField.value !== null)
      this.updateDayDateChange.emit(this.dayDateField.value);
  }

  //  When the field is required and the calendar is opened by clicking on the input (not from the calendar button)
  //  the input control loses focus and the input automatically becomes invalid before the user selects any value from the calendar.
  //  To avoid validation until the calendar loses focus, the required validation is temporarily removed
  onMouseDown(): void {
    if (
      this.isRequired &&
      !this.dayDateField.value &&
      this.dayDateField.untouched
    )
      this.enableRequiredValidator(false);
  }
  onClosed(): void {
    if (this.validatorRemoved) this.enableRequiredValidator(true);
  }

  enableRequiredValidator(value: boolean): void {
    if (value) this.dayDateField.addValidators([Validators.required]);
    else this.dayDateField.removeValidators([Validators.required]);

    this.dayDateField.updateValueAndValidity();
    this.validatorRemoved = !value;
  }
}
