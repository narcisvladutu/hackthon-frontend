import {
  Component,
  HostListener,
  Input,
  OnInit,
  Renderer2,
} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
})
export class UserProfileComponent implements OnInit {
  @Input() username: string | null = null;
  @Input() image: string | null = null;

  wasInside: boolean = false;

  listenerFn: (() => void) | undefined;

  constructor(private renderer2: Renderer2, private router: Router) {}

  ngOnInit(): void {}

  @HostListener('click')
  clickInside(): void {
    this.wasInside = true;
  }

  clickOut(): void {}

  onOpened(): void {
    this.listenerFn = this.renderer2.listen('document', 'click', () =>
      this.clickOut()
    );
  }

  onClosed(): void {}

  logout(): void {
    localStorage.removeItem('userData');
    localStorage.removeItem('tokenData');
    this.router.navigateByUrl('/log-in');
  }
}
