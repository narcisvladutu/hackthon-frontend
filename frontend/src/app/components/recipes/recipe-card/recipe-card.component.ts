import { Component, Input, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { Recipe } from 'src/app/models/Recipe';

@Component({
  selector: 'app-recipe-card',
  templateUrl: './recipe-card.component.html',
  styleUrls: ['./recipe-card.component.scss'],
})
export class RecipeCardComponent implements OnInit {
  @Input()
  recipe: Recipe | undefined;

  constructor(private router: Router) {}

  ngOnInit(): void {}

  navigateToRecipe() {
    localStorage.setItem(
      'recipeImage',
      this.recipe?.image ? this.recipe.image : ''
    );
    localStorage.setItem(
      'recipeDescription',
      this.recipe?.description ? this.recipe.description : ''
    );
    localStorage.setItem(
      'recipeName',
      this.recipe?.name ? this.recipe.name : ''
    );
    this.router.navigateByUrl('recipe');
  }
}
