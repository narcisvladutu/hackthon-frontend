import { User } from './User';

export interface AuthResponseData {
  user: User;
  token: string;
  error: string;
}
