import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-add-product-to-shopping-list',
  templateUrl: './add-product-to-shopping-list.component.html',
  styleUrls: ['./add-product-to-shopping-list.component.scss'],
})
export class AddProductToShoppingListComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<AddProductToShoppingListComponent>
  ) {}

  ngOnInit(): void {}

  accept() {
    this.dialogRef.close('DA');
  }

  reject() {
    this.dialogRef.close('NU');
  }
}
