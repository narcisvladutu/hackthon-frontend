import { HttpClient } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Product } from 'src/app/models/Product';
import { DeleteProductDialogComponent } from '../delete-product-dialog/delete-product-dialog.component';

@Component({
  selector: 'app-my-product',
  templateUrl: './my-product.component.html',
  styleUrls: ['./my-product.component.scss'],
})
export class MyProductComponent implements OnInit {
  @Input()
  product: Product | undefined;

  constructor(private http: HttpClient, private dialog: MatDialog) {}

  ngOnInit(): void {}

  deleteProduct(product: Product) {
    let dialogRef = this.dialog.open(DeleteProductDialogComponent, {
      height: '250px',
      width: '450px',
      enterAnimationDuration: '400ms',
      exitAnimationDuration: '300ms',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result == 'DA')
        this.http
          .delete(
            'http://localhost:8081/products/delete/' +
              encodeURIComponent(product.id)
          )
          .subscribe();
      window.location.reload();
    });
  }
}
