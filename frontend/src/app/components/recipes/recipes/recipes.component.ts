import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Ingredient } from 'src/app/models/Ingredient';
import { IngredientService } from 'src/app/services/ingredient.service';
import { addSelectedIngredients } from 'src/app/store/products/products-actions';
import { AppState } from 'src/app/store/state/app.state';

@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.scss'],
})
export class RecipesComponent implements OnInit {
  ingredients: Ingredient[] | undefined;
  selectedIngredients: Ingredient[] = [];

  constructor(
    private ingredientService: IngredientService,
    private store: Store<AppState>,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.ingredientService.getAllIngredients().subscribe((ingredients) => {
      this.ingredients = ingredients;
    });
  }

  selectOrDeselect(ingredient: Ingredient): void {
    if (this.selectedIngredients.includes(ingredient))
      this.selectedIngredients = this.selectedIngredients.slice(
        this.selectedIngredients.indexOf(ingredient),
        this.selectedIngredients.indexOf(ingredient)
      );
    else this.selectedIngredients.push(ingredient);
  }

  generateReciepes() {
    this.store.dispatch(
      addSelectedIngredients({ selectedIngredients: this.selectedIngredients })
    );
    // this.router.navigateByUrl('/recipes-filtered');
  }
}
