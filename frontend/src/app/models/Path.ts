export enum Path {
  PRODUCTS = '/products',
  RECIPES = '/recipes',
  MY_SHOPPING_LIST = '/my-shopping-list',
  MY_COMPANY_PRODUCTS = '/my-products',
  ADD_PRODUCT = '/add-product',
  MY_ORDERS = '/my-orders',
}
