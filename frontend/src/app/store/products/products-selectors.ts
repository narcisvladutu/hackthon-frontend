import { IProductsState } from './products-state';
import { createSelector } from '@ngrx/store';
import { AppState } from '../state/app.state';

export const selectProducts = (state: AppState) => state.products;
export const selectAllProducts = createSelector(
  selectProducts,
  (state: IProductsState) => state.products
);
export const selectUser = createSelector(
  selectProducts,
  (state: IProductsState) => state.user
);
export const selectProductHash = createSelector(
  selectProducts,
  (state) => state.productHash
);
export const selectSelectedIngredients = createSelector(
  selectProducts,
  (state) => state.selectedIngredients
);
