import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-place-order-dialog',
  templateUrl: './place-order-dialog.component.html',
  styleUrls: ['./place-order-dialog.component.scss'],
})
export class PlaceOrderDialogComponent implements OnInit {
  constructor(public dialogRef: MatDialogRef<PlaceOrderDialogComponent>) {}

  ngOnInit(): void {}

  accept() {
    this.dialogRef.close('DA');
  }

  reject() {
    this.dialogRef.close('NU');
  }
}
