import { UntilDestroy } from '@ngneat/until-destroy';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { UserRole } from 'src/app/models/UserRole.enum';
import { AuthResponseData } from 'src/app/models/AuthResponseData';
import { AppState } from 'src/app/store/state/app.state';
import { Store } from '@ngrx/store';
import { addUser } from 'src/app/store/products/products-actions';

@UntilDestroy()
@Component({
  selector: 'app-login',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss'],
})
export class LogInComponent implements OnInit {
  login: FormGroup;
  authError: boolean | undefined;
  loading: boolean | undefined;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: HttpClient,
    private store: Store<AppState>
  ) {
    this.login = new FormGroup({
      user: new FormControl(),
      password: new FormControl(),
    });
  }

  ngOnInit(): void {}

  clickLogin(): void {
    const session = {
      username: this.login.value.user,
      password: this.login.value.password,
    };
    this.http
      .post<AuthResponseData>('http://localhost:8081/auth/login', session)
      .subscribe((data) => {
        if (data.error !== null) {
          this.authError = true;
        }
        localStorage.setItem('userData', JSON.stringify(data.user));
        localStorage.setItem('tokenData', data.token);
        localStorage.setItem('userImage', data.user.image);
        localStorage.setItem('userName', data.user.username);
        localStorage.setItem('userRole', data.user.role.toString());
        localStorage.setItem('userId', data.user.id.toString());
        this.store.dispatch(addUser({ user: data.user }));
        if (data.user.role.toString() == 'ADMIN') {
          this.router.navigateByUrl('/my-products');
        } else {
          this.router.navigateByUrl('/products');
        }
      });
  }
}
