import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../../store/state/app.state';
import { Router } from '@angular/router';
import { FormBuilder, FormControl } from '@angular/forms';
import { OrderModel, OrderModelUpdate } from '../../../models/order.model';
import { Product } from '../../../models/Product';
import { selectProductHash } from '../../../store/products/products-selectors';
import { HttpClient } from '@angular/common/http';
import * as moment from 'moment';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { clearShoppingList } from 'src/app/store/products/products-actions';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss'],
})
export class OrdersComponent implements OnInit {
  checkoutForm = this.formBuilder.group({
    phone: new FormControl(''),
    firstName: new FormControl(''),
    lastName: new FormControl(''),
    country: new FormControl(''),
    town: new FormControl(''),
    state: new FormControl(''),
  });
  order: OrderModel = {
    phone: '',
    firstName: '',
    lastName: '',
    country: '',
    town: '',
    state: '',
  };

  productsHash: Map<string, Product[]> | null | undefined;

  total = 0;
  noOfProducts = 0;

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private dialog: MatDialog
  ) {}

  ngOnInit(): void {
    this.store.select(selectProductHash).subscribe((data) => {
      this.productsHash = data;
      let keys = this.getKeys(this.productsHash);
      if (keys) {
        for (let key of keys) {
          let products = this.productsHash?.get(key);
          if (products)
            for (let product of products) {
              this.total = this.total + product.price;
              this.noOfProducts++;
            }
        }
      }
    });
  }

  onSubmit() {
    this.order = Object.assign(this.order, this.checkoutForm.value);
    this.checkoutForm.reset();
  }

  getKeys(productsHash: Map<string, Product[]> | null | undefined) {
    if (productsHash) {
      return Array.from(productsHash.keys());
    }
    return null;
  }

  placeOrder() {
    let dialogRef = this.dialog.open(ConfirmDialogComponent, {
      height: '250px',
      width: '400px',
      enterAnimationDuration: '400ms',
      exitAnimationDuration: '300ms',
    });

    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'DA') {
        let keys = this.getKeys(this.productsHash);
        if (keys) {
          for (let key of keys) {
            let products = this.productsHash?.get(key);
            let productsWithStock: Map<string, number> = new Map<
              string,
              number
            >();
            products?.forEach((product) => {
              if (
                productsWithStock.get(product.id.toString()) &&
                productsWithStock
              ) {
                let x = productsWithStock.get(product.id.toString());
                if (x) {
                  x = x + 1;
                  productsWithStock.set(product.id.toString(), x);
                }
              } else {
                productsWithStock.set(product.id.toString(), 1);
              }
            });
            let convertMap = {};
            productsWithStock.forEach((key, value) => {
              convertMap = { ...convertMap, [value]: key };
            });
            let saveOrder = {
              name:
                localStorage.getItem('userName')! +
                ' ' +
                moment().format('DD/MM/YYYY HH:mm'),
              description: 'niciuna',
              status: 'INPROGRESS',
              createdDate: Date.now(),
              products: convertMap,
              username: localStorage.getItem('userName'),
              company: key,
            };
            this.http
              .post('http://localhost:8081/order/add', saveOrder)
              .subscribe();
          }
        }
        this.store.dispatch(clearShoppingList(''));
        this.router.navigateByUrl('/products');
      }
    });
  }
}
