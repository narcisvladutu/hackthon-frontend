import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Moment } from 'moment';
import { ProductSave } from 'src/app/models/ProductSave';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss'],
})
export class AddProductComponent implements OnInit {
  form: FormGroup | undefined;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private http: HttpClient
  ) {}

  ngOnInit(): void {
    this.form = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(5)]],
      image: ['', [Validators.required]],
      price: [
        0,
        [Validators.required, Validators.pattern('^\\d+$'), Validators.min(1)],
      ],
      stock: [
        0,
        [Validators.required, Validators.pattern('^\\d+$'), Validators.min(1)],
      ],
      expire_date: [null, [Validators.required]],
    });
  }

  goBack() {
    this.router.navigateByUrl('/my-products').then();
  }

  save() {
    if (this.form?.valid) {
      let newProduct: ProductSave = {
        ...this.form.value,
        companyName: localStorage.getItem('userName'),
      };
      this.http
        .post('http://localhost:8081/products/add', newProduct)
        .subscribe();
      this.router
        .navigateByUrl('/my-products')
        .then(() => window.location.reload());
    } else {
      alert('DATELE DUMNEAVOASTRA NU SUNT VALIDE!');
    }
  }

  onUpdateExpirationDate($event: Moment): void {
    if (this.form)
      if (this.form.get('expire_date') !== null)
        this.form.get('expire_date')?.setValue($event);
  }

  getErrorForImage() {
    if (this.form)
      if (this.form.get('image') !== null)
        return this.form.get('image')?.hasError('required');
    return false;
  }

  getErrorForPrice() {
    if (this.form)
      if (this.form.get('price') !== null)
        return (
          this.form.get('price')?.hasError('required') ||
          this.form.get('stock')?.hasError('min')
        );
    return false;
  }

  getErrorForStock() {
    if (this.form)
      if (this.form.get('stock') !== null)
        return (
          this.form.get('stock')?.hasError('required') ||
          this.form.get('stock')?.hasError('min')
        );
    return false;
  }

  getErrorForExpireDate() {
    if (this.form)
      if (this.form.get('expire_date') !== null)
        return this.form.get('expire_date')?.hasError('required');
    return false;
  }

  getErrorForName() {
    if (this.form)
      if (this.form.get('name') !== null)
        return (
          this.form.get('name')?.hasError('required') ||
          this.form.get('name')?.hasError('minlength')
        );
    return false;
  }
}
